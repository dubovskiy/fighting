import View from './view';
import ArenaFightingView from './arenaFightingView';

class ArenaButtonView extends View {
    constructor(arenaModel, type) {
        super();
        this.arena = arenaModel;
        this.createButton(type);
    }

    createButton(type){
        this.button = this.createElement({ tagName: 'button', className: 'fight' });
        this.button.innerHTML = `Go to Arena! (${type})`;
        this.buttonStateListener();
        this.arena.setCountListener(this.buttonStateListener.bind(this));
        this.button.addEventListener('click', ()=>{new ArenaFightingView(this.arena, type)});
    }

    enableButton(){
        this.button.disabled = false;
    }

    disableButton(){
        this.button.disabled = true;
    }

    buttonStateListener(){
        if (this.arena.notFilled()) {
            this.disableButton();
        } else {
            this.enableButton();
        }
    }
}


export default ArenaButtonView;