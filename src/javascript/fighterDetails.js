import View from "./view";

class Details extends View {
    constructor(arena){
        super();
        this.arena = arena;
        this.element = this.createElement({tagName: 'div', className: 'fighter-details modal'});
        document.body.append(this.element);
        this.hide();
    }

    show(){
        this.element.classList.remove('hide');
    }

    hide(){
        this.element.classList.add('hide');
        this.element.innerHTML = '';
    }

    initArenaState(details, arenaBtn, fighterBlock){
        if (this.arena.isAssigned(details) !== -1) {
            fighterBlock.classList.add('arena-ready');
            arenaBtn.innerHTML = 'remove from arena';
        } else if (this.arena.notFilled()) {
            arenaBtn.innerHTML = 'add to arena';
        } else {
            arenaBtn.innerHTML = 'arena is fullfilled';
            arenaBtn.disabled = 'disabled';
        }
    }

    showDetails(event, details, fighterBlock) {
        this.element.innerHTML = `
            <div class="modal-content-wrapper">
          <div class="fighter-name">${details.name}</div>
          <div class="fighter-image"><img src="${details.source}"></div>
          <div class="fighter-health">Health: <span class="value">${details.health}</span> <div class="range">0 <input type="range" value="${details.health}" min="20" max="100"/> 100</div></div>
          <div class="fighter-attack">Attack: <span class="value">${details.attack}</span> <div class="range">0 <input type="range" value="${details.attack}" min="0" max="7"/> 7</div></div>
          <div class="fighter-defense">defense: <span class="value">${details.defense}</span> <div class="range">0 <input type="range" value="${details.defense}" min="0" max="7"/> 7</div></div>
          <div class="actions"><button class="saveBtn">Save</button> <button class="cancelBtn">Cancel</button> <button class="arenaBtn"></button></div>
          </div>
        `;
        this.show();

        const arenaBtn = this.element.querySelector('.arenaBtn');
        this.initArenaState(details, arenaBtn, fighterBlock);

        this.valueHandler(this.element.querySelector('.fighter-health input'), this.element.querySelector('.fighter-health .value'));
        this.valueHandler(this.element.querySelector('.fighter-attack input'), this.element.querySelector('.fighter-attack .value'));
        this.valueHandler(this.element.querySelector('.fighter-defense input'), this.element.querySelector('.fighter-defense .value'));


        arenaBtn.addEventListener('click', () => {
            this.arena.toggleFighter(details);
            this.initArenaState(details, arenaBtn, fighterBlock);
            this.hide();
        });

        this.element.querySelector('.saveBtn').addEventListener('click', () => {
            details.health = +this.element.querySelector('.fighter-health input').value;
            details.attack = +this.element.querySelector('.fighter-attack input').value;
            details.defense = +this.element.querySelector('.fighter-defense input').value;
            this.arena.updateFighter(details);
            this.hide();
        });


        this.element.querySelector('.cancelBtn').addEventListener('click', () => {
            this.hide();
        });
    }
}

export default Details