import FightersView from './fightersView';
import Arena from './arena';
import ArenaButtonView from './arenaButtonView';
import { fighterService } from './services/fightersService';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
        const arena = new Arena();
        const fightersView = new FightersView(fighters, arena);
        const fightersElement = fightersView.element;
        const arenaButtonView = new ArenaButtonView(arena, 'sync');
        const arenaButtonView2 = new ArenaButtonView(arena, 'async');

      App.rootElement.appendChild(fightersElement);
        App.rootElement.appendChild(arenaButtonView.button);
        App.rootElement.appendChild(arenaButtonView2.button);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;