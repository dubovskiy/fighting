import View from './view';

class ArenaFightingView extends View {
    constructor(arenaModel, type) {
        super();
        this.arena = arenaModel;
        this.arena.fight(type);
    }

}

export default ArenaFightingView;