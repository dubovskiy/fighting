class Fighter {
    constructor(fighterDetail) {
        this.name = fighterDetail.name;
        this.attack = fighterDetail.attack;
        this.defense = fighterDetail.defense;
        this.health = fighterDetail.health;
    }

    getHitPower() {
        const criticalHitChance = 1 + Math.random();
        return this.attack * criticalHitChance;
    }

    getBlockPower() {
        const dodgeChance = 1 + Math.random();
        return this.defense * dodgeChance;
    }

    setHit(hit, block) {
        if (hit > block) {
            this.health -= hit - block;
        }
    }

    isDead() {
        return this.health <=  0;
    }
}

export default Fighter;