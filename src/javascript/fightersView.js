import View from './view';
import FighterView from './fighterView';
import Details from './fighterDetails';
import { fighterService } from './services/fightersService';

class FightersView extends View {
  constructor(fighters, arena) {
    super();
      this.arena = arena;
      this.handleClick = this.handleFighterClick.bind(this);
      this.detailsBlock = new Details(this.arena);

      this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter, fighterBlock) {
    if (!this.fightersDetailsMap.get(fighter._id)) {
      fighter.details = await fighterService.getFighterDetails(fighter._id);
      this.fightersDetailsMap.set(fighter._id, fighter);
    }

    console.log('clicked');
    this.detailsBlock.showDetails(event, fighter.details, fighterBlock);

    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  }
}


export default FightersView;