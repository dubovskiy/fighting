import Fighter from "./fighter";

class Arena{
    constructor(){
        this.opponents = [];
    }

    countListener = [];

    isAssigned(fighterDetails) {
        let index = -1;
        this.opponents.some((opponent, indexOp) => {
            if (opponent._id === fighterDetails._id){
                index = indexOp;
            }
        });
        return index;
    }

    notFilled(){
        return this.opponents.length < 2;
    }

    toggleFighter(fighterDetails){
        const index = this.isAssigned(fighterDetails);
        if (index === -1) {
            this.opponents.push({...fighterDetails});
        } else {
            this.opponents.splice(index, 1);
        }
        this.countListener.forEach((listener) => listener());
    }

    updateFighter(fighterDetails) {
        const index = this.isAssigned(fighterDetails);
        if (index !== -1) {
            this.opponents.splice(index, 1);
            this.opponents.push({...fighterDetails});
        }
    }

    setCountListener(callback){
        this.countListener.push(callback);
    }

    fightStep(fighter1, fighter2) {
        const hit = fighter1.getHitPower();
        const block = fighter2.getBlockPower();
        fighter2.setHit(hit, block);
        console.log(`${fighter1.name}(health: ${fighter1.health.toFixed(1)}) hit ${fighter2.name}(health: ${fighter2.health.toFixed(1)}) with power ${hit.toFixed(1)} and block ${block.toFixed(1)} .`);
        return fighter2.isDead();
    }




    async asyncStep(fighters, resolve) {
        const time = Math.random() * 200 * fighters[0].attack;
        setTimeout(()=>{
            if (fighters.length > 1 && this.fightStep(...fighters)){
                fighters.splice(1,1);
                resolve();
            } else if (fighters.length > 1){
                this.asyncStep(fighters, resolve);
            } else {
                resolve();
            }
        }, time);
    }

    async asyncStep2(fighters, resolve) {
        const time = Math.random() * 200 * fighters[1].attack;
        setTimeout(()=>{
            if (fighters.length > 1 && this.fightStep(fighters[1], fighters[0])){
                fighters.splice(0, 1);
                resolve();
            } else if (fighters.length > 1){
                this.asyncStep2(fighters, resolve);
            } else {
                resolve();
            }
        }, time);
    }


    startASync(fighters) {
        return new Promise(resolve => {
            this.asyncStep(fighters, resolve);
            this.asyncStep2(fighters, resolve);
        });
    }

    startSync(fighters) {
        return new Promise(resolve => {
            const interval = setInterval(() => {
                if (fighters.length > 1) {
                    if (this.fightStep(fighters[0], fighters[1])) {
                        fighters.splice(1, 1);
                    }
                    fighters.reverse();
                } else {
                    clearInterval(interval);
                    resolve();
                }
            }, 1000);
        });
    }


    fight (fightType = 'sync') {
        const fighters = this.opponents.map((opponent) => new Fighter(opponent));
        if (fightType === 'sync') {
            this.startSync(fighters).then(()=> { alert(fighters[0].name + ' WON!!!') });
        } else if (fightType === 'async') {
            this.startASync(fighters).then(()=> { alert(fighters[0].name + ' WON!!!') });
        }
    }
}

export default Arena;