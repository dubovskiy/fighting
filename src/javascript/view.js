class View {
  element;

  createElement({ tagName, className = '', attributes = {} }) {
    const element = document.createElement(tagName);
      className.split(' ').forEach((oneName) => {
          element.classList.add(oneName);
      });
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
  }

  valueHandler(source, target) {
    source.addEventListener('input', () => target.innerHTML = source.value);
  }
}

export default View;
